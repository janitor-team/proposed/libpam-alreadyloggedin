libpam-alreadyloggedin for Debian
=================================

To enable the alreadyloggedin module, add the following line to
/etc/pam.d/login, just before ``@include common-auth``::

   auth sufficient pam_alreadyloggedin.so no_root restrict_tty=/dev/tty[0-9]* restrict_loggedin_tty=/dev/tty[0-9]*


Security notes
--------------
libpam-alreadyloggedin uses utmp login records to determine whether a user is
logged in or not.

Terminal locking programs (e.g., vlock) don't remove utmp entries, thus a user
would be able to log in without password even if he/she locked all terminals
he/she were using. To use vlock along with libpam-alreadyloggedin securely,
always use the -a/--all option.

